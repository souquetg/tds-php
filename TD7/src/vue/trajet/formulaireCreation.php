<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Création d'un Trajet</title>
</head>
<body>
<div>
    <form method="get" action="controleurFrontal.php">
        <!-- Champs cachés pour le contrôleur et l'action -->
        <input type="hidden">
        <input type="hidden" name="action" value="creerDepuisFormulaire">

        <fieldset>
            <legend>Mon formulaire de création de trajet :</legend>
            <p>
                <label for="depart_id">Départ</label> :
                <input type="text" placeholder="Montpellier" name="depart" id="depart_id" required/>
            </p>
            <p>
                <label for="arrivee_id">Arrivée</label> :
                <input type="text" placeholder="Sète" name="arrivee" id="arrivee_id" required/>
            </p>
            <p>
                <label for="date_id">Date</label> :
                <input type="date" placeholder="JJ/MM/AAAA" name="date" id="date_id" required/>
            </p>
            <p>
                <label for="prix_id">Prix</label> :
                <input type="number" step="0.01" placeholder="20" name="prix" id="prix_id" required/>
            </p>
            <p>
                <label for="conducteurLogin_id">Login du conducteur</label> :
                <input type="text" placeholder="leblancj" name="conducteurLogin" id="conducteurLogin_id" required/>
            </p>
            <p>
                <label for="nonFumeur_id">Non Fumeur ?</label> :
                <input type="hidden" name="nonFumeur" value="0"/>
                <input type="checkbox" name="nonFumeur" id="nonFumeur_id" value="1"/>

            </p>
            <p>
                <input type="submit" value="Envoyer" />
            </p>
        </fieldset>
    </form>
</div>
</body>
</html>