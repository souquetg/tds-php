<?php
use App\Covoiturage\Lib\PreferenceControleur;

$controleurDefaut = PreferenceControleur::existe() ? PreferenceControleur::lire() : '';
?>

<form method="get" action="controleurFrontal.php">
    <input type='hidden' name='action' value='enregistrerPreference'/>
    <fieldset>
        <legend>Préférence de l'utilisateur</legend>

        <p class="InputAddOn">
            <label class="InputAddOn-field" for="utilisateurId">Utilisateur</label>
            <input class="InputAddOn-item" type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur"
                <?= $controleurDefaut === 'utilisateur' ? 'checked' : '' ?>>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-field" for="trajetId">Trajet</label>
            <input class="InputAddOn-item" type="radio" id="trajetId" name="controleur_defaut" value="trajet"
                <?= $controleurDefaut === 'trajet' ? 'checked' : '' ?>>
        </p>

        <p>
            <input type="submit" value="Envoyer"/>
        </p>
    </fieldset>
</form>
