<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use Exception;

class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer();//appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('vueGenerale.php', [
            'utilisateurs' => $utilisateurs,
            "titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/liste.php"]);
    }

    /**
     * @throws Exception
     */
    public static function afficherDetail(): void
    {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($utilisateur == null) {
            ControleurUtilisateur::afficherErreur($login);
        } else {
            ControleurUtilisateur::afficherVue( "vueGenerale.php",
                ["titre" => "Detail utilisateur",
                    "cheminCorpsVue" => 'utilisateur/detail.php',
                    'utilisateur' => $utilisateur]);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        ControleurUtilisateur::afficherVue( "vueGenerale.php",
            ["titre" => "Ajout Utilisateur",
            "cheminCorpsVue" => 'utilisateur/formulaireCreation.php']);
    }

    public static function creerDepuisFormulaire(): void
    {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];

        // Créer un nouvel objet Utilisateur
        $utilisateur = new Utilisateur($login, $nom, $prenom);

        // Appeler la méthode ajouter() de UtilisateurRepository pour ajouter l'utilisateur
        (new UtilisateurRepository())->ajouter($utilisateur);

        // Récupérer tous les utilisateurs pour les afficher dans la vue
        $utilisateurs = (new UtilisateurRepository())->recuperer();

        // Appeler la vue pour afficher la liste avec le message d'utilisateur créé
        ControleurUtilisateur::afficherVue('vueGenerale.php', [
            'utilisateurs' => $utilisateurs,
            'titre' => 'Liste des utilisateurs',
            'cheminCorpsVue' => 'utilisateur/utilisateurCree.php'
        ]);
    }

    public static function afficherErreur(string $messageErreur = ""): void
    {
        if ($messageErreur == "") {
            $messageErreur = "Problème avec l'utilisateur.";
        } else {
            $messageErreur = "Problème avec l'utilisateur : " . $messageErreur;
        }

        // Appel de la vue d'erreur
        ControleurUtilisateur::afficherVue('vueGenerale.php', [
            'messageErreur' => $messageErreur,
            'titre' => 'Erreur utilisateur',
            'cheminCorpsVue' => 'utilisateur/erreur.php'
        ]);
    }

    public static function supprimerUtilisateur(): void
    {
        // Récupérer le login de l'utilisateur à supprimer
        $login = $_GET['login'];

        // Suppression de l'utilisateur via le repository
        (new UtilisateurRepository())->supprimer($login);

        // Récupérer la liste mise à jour des utilisateurs après suppression
        $utilisateurs = (new UtilisateurRepository())->recuperer();

        // Afficher la vue utilisateurSupprime avec le login et la listeafficherFormulaireMiseAJour des utilisateurs
        ControleurUtilisateur::afficherVue('vueGenerale.php', [
            'login' => $login,
            'utilisateurs' => $utilisateurs,
            'titre' => 'liste utilisateur',
            'cheminCorpsVue' => 'utilisateur/utilisateurSupprime.php'
        ]);
    }

    public static function afficherFormulaireMiseAJour(): void
    {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        ControleurUtilisateur::afficherVue('vueGenerale.php', [
            'utilisateur' => $utilisateur,
            'titre' => 'Mise à jour utilisateur',
            'cheminCorpsVue' => 'utilisateur/formulaireMiseAJour.php']);
    }

    public static function mettreAJour(): void{
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];

        $utilisateur = new Utilisateur($login, $nom, $prenom);

        UtilisateurRepository::mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();

        ControleurUtilisateur::afficherVue('vueGenerale.php', [
            'login' => $login,
            'utilisateur' => $utilisateur,
            'titre' => 'Mis a jour',
            'cheminCorpsVue' => 'utilisateur/utilisateurMisAJour.php',
            'utilisateurs' => $utilisateurs]);

    }

//    public static function deposerCookie(): void{
//        Cookie::enregistrer("TestCookie", "sdf", time() + 3600);
//    }
//
//    public static function lireCookie(): void{
//        echo Cookie::lire("TestCookie");
//
//    }
    public static function deposerSession(): void{
        session_start();
        $session = Session::getInstance();
        $session->enregistrer("TestSession", "sdf");
    }

    public static function lireSession(): void{
        $session = Session::getInstance();
        echo $session->lire("TestSession");
    }

    public static function supprimerSession(): void{
        $session = Session::getInstance();
        $session->detruire();
    }

}?>
