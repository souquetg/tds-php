<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;

abstract class AbstractRepository
{
    public  function ajouter(AbstractDataObject $objet): bool
    {
        $nomsColonnes = $this->getNomsColonnes();

        // Création des tags pour la requête SQL
        $tags = array_map(function($colonne) {
            return ':' . $colonne . 'Tag';  // Exemple : :loginTag, :nomTag, etc.
        }, $nomsColonnes);

        // Construction de la requête SQL
        $requete = "INSERT INTO " . $this->getNomTable() . " (" . implode(", ", $nomsColonnes) . ") VALUES (" . implode(", ", $tags) . ")";

        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($requete);

        // Association des valeurs
        $values = $this->formatTableauSQL($objet);
        $pdoStatement->execute($values);

        return $pdoStatement->rowCount() > 0;
    }

    public function supprimer($valeurClePrimaire): void
    {
        $sql = "DELETE FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :clePrimaireTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = ["clePrimaireTag" => $valeurClePrimaire];
        $pdoStatement->execute($values);
    }

    public function recupererParClePrimaire(string $clePrimaire): ?AbstractDataObject
    {
        $sql = "SELECT * FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :clePrimaireTag";

        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        // Association de la clé primaire
        $values = ["clePrimaireTag" => $clePrimaire];

        // Exécution de la requête
        $pdoStatement->execute($values);

        // Récupération du résultat sous forme de tableau
        $objetFormatTableau = $pdoStatement->fetch();

        // Si aucun résultat n'est trouvé
        if ($objetFormatTableau === false) {
            return null;
        }

        // Construction de l'objet à partir du tableau SQL
        return $this->construireDepuisTableauSQL($objetFormatTableau);
    }

    /**
     * @return AbstractDataObject[]
     */
    public function recuperer(): array
    {
        $tableau = array();
        $requete = "SELECT * FROM " . $this->getNomTable();
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query($requete);
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $tableau[] = $this->construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $tableau;
    }

    protected abstract function getNomTable(): string;

    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : ?AbstractDataObject;

    protected abstract function getNomClePrimaire(): string;

    protected abstract function getNomsColonnes(): array;

    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;



}