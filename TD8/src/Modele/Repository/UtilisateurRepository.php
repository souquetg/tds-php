<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use PDO;
class UtilisateurRepository extends AbstractRepository
{


    public function construireDepuisTableauSQL(array $objetFormatTableau): ?Utilisateur
    {
        return new Utilisateur(
            $objetFormatTableau["loginBaseDeDonnees"],
            $objetFormatTableau["nomBaseDeDonnees"],
            $objetFormatTableau["prenomBaseDeDonnees"],
            $objetFormatTableau["mdpHache"]
        );
    }

    private static function recupererTrajetsCommePassager(Utilisateur $utilisateur): array
    {
        $tab = array();
        $requete = "
        SELECT t.* 
        FROM trajet t 
        INNER JOIN passager p ON t.id = p.trajetId 
        WHERE p.passagerLogin = :login";
        $pdo = ConnexionBaseDeDonnees::getPDO()->prepare($requete);
        $values = array(
            'login' => $utilisateur->getLogin()
        );
        $pdo->execute($values);
        foreach ($pdo as $trajet) {
            $tab[] = (new TrajetRepository())->construireDepuisTableauSQL($trajet);
        }
        return $tab;
    }

    public static function mettreAJour(Utilisateur $utilisateur): void{
        $sql = "UPDATE utilisateur SET nomBaseDeDonnees = :nomTag, prenomBaseDeDonnees = :prenomTag WHERE loginBaseDeDonnees = :loginTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom()
        );
        $pdoStatement->execute($values);
    }

    public function getNomTable(): string
    {
        return "utilisateur";
    }

    public function getNomClePrimaire(): string
    {
        return "loginBaseDeDonnees";
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["loginBaseDeDonnees", "nomBaseDeDonnees", "prenomBaseDeDonnees", "mdpHache"];
    }

    protected function formatTableauSQL(AbstractDataObject $objet): array
    {
        /** @var Utilisateur $utilisateur */
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
            "mdpHacheTag" => $utilisateur->getMdpHache()
        );
    }





}