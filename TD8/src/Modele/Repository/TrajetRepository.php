<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Trajet;

use DateTime;
use Exception;
use PDO;
class TrajetRepository extends AbstractRepository
{
    protected function construireDepuisTableauSQL(array $objetFormatTableau): Trajet
    {

        $trajettab = new Trajet(
            $objetFormatTableau['id'],
            $objetFormatTableau['depart'],
            $objetFormatTableau['arrivee'],
            new DateTime($objetFormatTableau['date']),
            $objetFormatTableau['prix'],
            (new UtilisateurRepository())->recupererParClePrimaire($objetFormatTableau['conducteurLogin']),
            $objetFormatTableau['nonFumeur'],
            []
        );

        $trajettab->setPassagers(TrajetRepository::recupererPassagers($trajettab));
        return $trajettab;
    }

    public function supprimerPassager(string $passagerLogin): bool
    {
        $requete = "DELETE FROM passager WHERE passagerLogin = :passagerLogin AND trajetId = :trajetId";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($requete);

        $pdoStatement->execute([
            'passagerLogin' => $passagerLogin,
            'trajetId' => $this->id
        ]);

        return $pdoStatement->rowCount() > 0;
    }

    public static function recupererTrajetParId(int $trajetId): ?Trajet
    {
        $requete = "SELECT * FROM trajet WHERE id = :trajetId";
        $pdo = ConnexionBaseDeDonnees::getPDO()->prepare($requete);
        $pdo->execute(['trajetId' => $trajetId]);

        $trajetTableau = $pdo->fetch(PDO::FETCH_ASSOC);

        if ($trajetTableau) {
            // Retourne un trajet avec des passagers vides pour le moment
            return (new TrajetRepository())->construireDepuisTableauSQL($trajetTableau);
        } else {
            return null;
        }
    }


    static public function recupererPassagers(Trajet $trajet): array
    {
        $tab = array();
        $requete = "
        SELECT u.* 
        FROM utilisateur u 
        INNER JOIN passager p ON u.loginBaseDeDonnees = p.passagerLogin 
        WHERE p.trajetId = :trajetId";

        $pdo = ConnexionBaseDeDonnees::getPDO()->prepare($requete);
        $values = array(
            'trajetId' => $trajet->getId()
        );

        $pdo->execute($values);
        foreach ($pdo as $passager) {
            $tab[] = (new UtilisateurRepository())->construireDepuisTableauSQL($passager);
        }

        return $tab;
    }

//    /**
//     * @return Trajet[]
//     */
//    public static function recupererTrajets(): array
//    {
//        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");
//
//        $trajets = [];
//        foreach ($pdoStatement as $trajetFormatTableau) {
//            $trajets[] = TrajetRepository::construireDepuisTableauSQL($trajetFormatTableau);
//        }
//
//        return $trajets;
//    }

    public function getNomTable(): string
    {
        return "trajet";
    }

    public function getNomClePrimaire(): string
    {
        return "id";
    }

    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $objet): array
    {
        /** @var Trajet $objet */
        return [
            "idTag" => $objet->getId(),
            "departTag" => $objet->getDepart(),
            "arriveeTag" => $objet->getArrivee(),
            "dateTag" => $objet->getDate()->format("Y/m/d"),
            "prixTag" => $objet->getPrix(),
            "conducteurLoginTag" => $objet->getConducteur()->getLogin(),
            "nonFumeurTag" => $objet->isNonFumeur()
        ];
    }


}