<?php

namespace App\Covoiturage\Modele\HTTP;

class Cookie
{

    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void
    {
        $valeurs = json_encode($valeur);
        if ($dureeExpiration === null) {
            setcookie($cle, $valeurs, 0);
        } else {
            setcookie($cle, $valeurs, time() + $dureeExpiration);
        }
    }

    public static function lire(string $cle): mixed
    {
        if (self::contient($cle)) {
            return json_decode($_COOKIE[$cle], true);
        }
        return null;
    }

    public static function contient($cle) : bool
    {
        return isset($_COOKIE[$cle]);
    }

    public static function supprimer($cle) : void
    {
        unset($_COOKIE[$cle]);
        setcookie($cle, '', 1);
    }
}