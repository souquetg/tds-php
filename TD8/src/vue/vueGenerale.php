<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php
        /**
         * @var string $titre
         */
        echo $titre; ?></title>
    <link rel="stylesheet" href="../ressources/navstyle.css">
</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe">Gestion des utilisateurs</a>
            </li><li>
                <a href="controleurFrontal.php?action=afficherListe">Gestion des trajets</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherFormulairePreference" title="Préférences">
                    <img src="../ressources/img/heart.png" alt="Préférences" width="50" height="50">
                </a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherFormulaireCreation">
                    <img src="../ressources/img/add-user.png" alt="Ajouter" width="50" height="50">
                </a>
            </li>
        </ul>
    </nav>

</header>
<main>
    <?php
    /**
     * @var string $cheminCorpsVue
     */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <!-- Votre pied de page ici -->
    <p>Site de covoiturage de Gauthier Souquet</p>
</footer>
</body>
</html>

