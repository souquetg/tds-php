<?php
//affiche tout les détails de l'utilisateur stocké dans $utilisateur
use App\Covoiturage\Modele\DataObject\Utilisateur;
/** @var Utilisateur $utilisateur */
$loginHTML = $utilisateur->getLogin();
$nomHTML = $utilisateur->getNom();
$prenomHTML = $utilisateur->getPrenom();
echo '<p> Utilisateur de login ' . htmlspecialchars($loginHTML) . '.</p>';
echo '<p> Utilisateur de nom ' . htmlspecialchars($nomHTML) . '.</p>';
echo '<p> Utilisateur de prenom ' . htmlspecialchars($prenomHTML) . '.</p>';

?>

