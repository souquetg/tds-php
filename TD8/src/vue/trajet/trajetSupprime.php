<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Trajet Supprimé</title>
</head>
<body>
<h1>Trajet supprimé</h1>
<p>Le trajet avec l'ID <?= htmlspecialchars($_GET['id']) ?> a bien été supprimé.</p>

<?php
use App\Covoiturage\Modele\Repository\TrajetRepository;
// Récupérer la liste des trajets pour les afficher après la suppression
$trajets = (new TrajetRepository())->recuperer(); // Assurez-vous que cette méthode existe dans votre repository.

// Inclure la vue qui affiche la liste des trajets
require 'liste.php';
?>
</body>
</html>