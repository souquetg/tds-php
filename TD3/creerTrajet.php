<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<p>La donnée est
    <?php
    require_once("Utilisateur.php");
    require_once("ConnexionBaseDeDonnees.php");
    require_once ("Trajet.php");

    $utilisateurs = Utilisateur::getUtilisateurParLogin($_GET['conducteurLogin']);

    $depart = $_GET['depart'];
    $arrivee = $_GET['arrivee'];
    $date = new DateTime($_GET['date']);
    $prix = $_GET['prix'];
    $conducteur = $utilisateurs;
    $nonFumeur = $_GET['nonFumeur'] ;

    $trajet = new Trajet(null, $depart, $arrivee, $date, $prix, $utilisateurs, $nonFumeur, array());

    $trajet->ajouter();
    echo $trajet;
    ?>
</p>
</body>
</html>


