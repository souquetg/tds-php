<?php
require_once 'Trajet.php';
class Utilisateur {



    private string $login;
    private string $nom;
    private string $prenom;
    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;
    // un getter
    public function getNom(): string {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom): void {
        $this->nom = $nom;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        return new Utilisateur(
            $utilisateurFormatTableau['loginBaseDeDonnees'],
            $utilisateurFormatTableau['nomBaseDeDonnees'],
            $utilisateurFormatTableau['prenomBaseDeDonnees']
        );
    }

    public static function recupererUtilisateur(): array
    {
        $tableau = array();
        $requete = "SELECT * FROM utilisateur";
        $pdo = ConnexionBaseDeDonnees::getPdo()->prepare($requete);
        $pdo->execute();
        $pdoStatement = $pdo->fetchAll(PDO::FETCH_ASSOC);
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $tableau[] = Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $tableau;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom,
        array $trajetsCommePassager = null
   ) {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString(): string {
        return $this->login . " " . $this->nom . " " . $this->prenom;
    }
    /**
     * @return Trajet[] retournera les trajets auxquels l’utilisateur courant est inscrit en tant que passager
     */
    private function recupererTrajetsCommePassager() : array {
        $tab = array();
        $requete = "
        SELECT t.* 
        FROM trajet t 
        INNER JOIN passager p ON t.id = p.trajetId 
        WHERE p.passagerLogin = :login";
        $pdo = ConnexionBaseDeDonnees::getPDO()->prepare($requete);
        $values = array(
            'login' => $this->login
        );
        $pdo->execute($values);
        foreach ($pdo as $trajet) {
            $tab[] = Trajet::construireDepuisTableauSQL($trajet);
        }
        return $tab;
    }

    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager === null) {
            $this->trajetsCommePassager = $this->recupererTrajetsCommePassager();
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }


    /**
     * @return mixed
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    public static function getUtilisateurParLogin(string $login) : Utilisateur {
        $sql = "SELECT * from utilisateur WHERE loginBaseDeDonnees = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête

        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if ($utilisateurFormatTableau === false) {
            throw new Exception("L'utilisateur $login n'existe pas.");
        }
        return Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }
    public function ajouter() : void
    {
        $sql = "INSERT INTO utilisateur (loginBaseDeDonnees, nomBaseDeDonnees, prenomBaseDeDonnees) VALUES (:loginTag, :nomTag, :prenomTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $this->login,
            "nomTag" => $this->nom,
            "prenomTag" => $this->prenom
        );
        $pdoStatement->execute($values);


    }



}

?>

