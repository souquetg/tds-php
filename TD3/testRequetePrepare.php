<?php
// Inclusion de la classe Utilisateur et de la classe de connexion à la base de données
require_once 'Utilisateur.php';
require_once 'ConnexionBaseDeDonnees.php';

try {
    // Création d'une instance de l'utilisateur et récupération par login
    $nouvelUtilisateur = new Utilisateur('nouveau_login', 'NomUtilisateur', 'PrenomUtilisateur');

    // Insertion dans la base de données
    $nouvelUtilisateur->ajouter();

} catch (PDOException $e) {
    echo 'Erreur : ' . $e->getMessage();
}

?>