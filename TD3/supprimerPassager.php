<?php
require_once 'Trajet.php';
require_once 'Utilisateur.php';
require_once 'ConnexionBaseDeDonnees.php';

// Récupération des paramètres dans l'URL
if (isset($_GET['login']) && isset($_GET['trajet_id'])) {
    $login = $_GET['login'];
    $trajetId = (int)$_GET['trajet_id'];

    // On récupère le trajet correspondant via une méthode à implémenter
    $trajet = Trajet::recupererTrajetParId($trajetId);

    if ($trajet) {
        // On tente de supprimer le passager
        $trajet->supprimerPassager($login);
        echo "Passager supprimé avec succès.";
    } else {
        echo "Erreur : Trajet non trouvé.";
    }
} else {
    echo "Paramètres manquants.";
}
?>