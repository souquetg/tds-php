<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php
/** @var ModeleUtilisateur[] $utilisateurs */

use App\Covoiturage\Modele\ModeleUtilisateur;

foreach ($utilisateurs as $utilisateur) {
    //les mettres en lien cliquable
    $loginHTML = $utilisateur->getLogin();
    $loginURL = $utilisateur->getLogin();
    echo '<a href="controleurFrontal.php?action=afficherDetail&login=' . rawurlencode($loginURL) . '">' . htmlspecialchars($loginHTML) . '</a><br>';
}
echo '<br>';
echo '<a href="controleurFrontal.php?action=afficherFormulaireCreation">Créer un utilisateur</a>'
?>
</body>
</html>
