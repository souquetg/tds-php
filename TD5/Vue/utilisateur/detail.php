<?php
//affiche tout les détails de l'utilisateur stocké dans $utilisateur

/** @var ModeleUtilisateur $utilisateur */

use App\Covoiturage\Modele\ModeleUtilisateur;
$loginHTML = $utilisateur->getLogin();
$nomHTML = $utilisateur->getNom();
$prenomHTML = $utilisateur->getPrenom();
echo '<p> Utilisateur de login ' . htmlspecialchars($loginHTML) . '.</p>';
echo '<p> Utilisateur de nom ' . htmlspecialchars($nomHTML) . '.</p>';
echo '<p> Utilisateur de prenom ' . htmlspecialchars($prenomHTML) . '.</p>';

?>

