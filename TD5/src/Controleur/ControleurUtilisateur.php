<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\ModeleUtilisateur as ModeleUtilisateur;
use Exception;

class ControleurUtilisateur
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
    }

    /**
     * @throws Exception
     */
    public static function afficherDetail(): void
    {
        $login = $_GET['login'];
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login);
        if ($utilisateur == NULL) {
            ControleurUtilisateur::afficherVue('utilisateur/erreur.php');
        } else {
            ControleurUtilisateur::afficherVue( 'utilisateur/detail.php', ['utilisateur' => $utilisateur]);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        ControleurUtilisateur::afficherVue( 'utilisateur/formulaireCreation.php');
    }


    private static function afficherVue(string $cheminVue, array $parametres = []) : void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../../Vue/$cheminVue"; // Charge la vue
    }

    public static function creerDepuisFormulaire(): void
    {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        ControleurUtilisateur::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }

}

?>
