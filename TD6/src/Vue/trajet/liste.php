<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des trajets</title>
</head>
<body>
<h1>Liste des trajets</h1>
<?php
use App\Covoiturage\Modele\DataObject\Trajet;
/** @var Trajet[] $trajets */

if (!empty($trajets)) {
    foreach ($trajets as $trajet) {
        // Créer un affichage lisible pour le trajet
        $trajetHTML = htmlspecialchars($trajet->getDepart()) . " - " . htmlspecialchars($trajet->getArrivee());

        // Afficher les liens pour voir les détails, supprimer et mettre à jour
        echo '<p>';
        echo '<a href="controleurFrontal.php?controleur=trajet&action=afficherDetail&id=' . htmlspecialchars($trajet->getId()) . '">Voir le détail de ' . $trajetHTML . '</a><br>';
        echo '<a href="controleurFrontal.php?controleur=trajet&action=supprimerTrajet&id=' . htmlspecialchars($trajet->getId()) . '">Supprimer ' . $trajetHTML . '</a><br>';
        echo '<a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireMiseAJour&id=' . htmlspecialchars($trajet->getId()) . '">Mettre à jour ' . $trajetHTML . '</a><br>';
        echo '</p>';
    }
} else {
    echo '<p>Aucun trajet disponible.</p>';
}

echo '<p><a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireCreation">Ajouter un trajet</a></p>';
?>
</body>
</html>