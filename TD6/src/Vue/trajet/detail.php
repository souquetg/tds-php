<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Détails du trajet</title>
</head>
<body>
<h1>Détails du trajet</h1>

<?php
use App\Covoiturage\Modele\DataObject\Trajet;
/** @var Trajet $trajet */
$departHTML = $trajet->getDepart();
$arriveeHTML = $trajet->getArrivee();
$dateHTML = $trajet->getDate()->format('d/m/Y');
$prixHTML = $trajet->getPrix();
$conducteurHTML = $trajet->getConducteur()->getPrenom() . ' ' . $trajet->getConducteur()->getNom();
$nonFumeurHTML = $trajet->isNonFumeur() ? 'non fumeur' : 'fumeur';
echo '<p> Trajet de ' . htmlspecialchars($departHTML) . ' à ' . htmlspecialchars($arriveeHTML) . '.</p>';
echo '<p> Date du trajet : ' . htmlspecialchars($dateHTML) . '.</p>';
echo '<p> Prix du trajet : ' . htmlspecialchars($prixHTML) . '.</p>';
echo '<p> Conducteur du trajet : ' . htmlspecialchars($conducteurHTML) . '.</p>';
echo '<p> Ce trajet est ' . htmlspecialchars($nonFumeurHTML) . '.</p>';
?>
    <h2>Passagers</h2>
    <p>
        <?php
        $passagers = $trajet->getPassagers();
        if (count($passagers) === 0) {
            echo 'Aucun passager pour ce trajet.';
        } else {
            echo 'Passagers pour ce trajet :';
            foreach ($passagers as $passager) {
                echo '<br>' . $passager->getPrenom() . ' ' . $passager->getNom();
            }
        }
?>
    </p>
</body>
</html>