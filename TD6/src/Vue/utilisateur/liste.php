<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php
/** @var Utilisateur[] $utilisateurs */

use App\Covoiturage\Modele\DataObject\Utilisateur;

foreach ($utilisateurs as $utilisateur) {
    //les mettres en lien cliquable
    $loginHTML = $utilisateur->getLogin();
    $loginURL = $utilisateur->getLogin();
    echo '<a href="controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=' . rawurlencode($loginURL) . '">' . htmlspecialchars($loginHTML)
        . '</a> <a href="controleurFrontal.php?controleur=utilisateur&action=supprimerUtilisateur&login=' . rawurlencode($loginURL) . '"><br> supprimer '
        .  htmlspecialchars($loginHTML) . ' </a> <a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . rawurlencode($loginURL) . '"><br>Mettre A Jour</a> <br><br>';
}
echo '<br>';
echo '<a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation">Créer un utilisateur</a>'
?>
</body>
</html>
