<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

use DateTime;
use Exception;

class ControleurTrajet
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $trajets = (new TrajetRepository())->recuperer();
        ControleurTrajet::afficherVue('vueGenerale.php', [
            'trajets' => $trajets,
            "titre" => "Liste des trajets",
            "cheminCorpsVue" => "trajet/liste.php"]);
    }


    private static function afficherVue(string $cheminVue, array $parametres = []) : void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../Vue/$cheminVue"; // Charge la vue
    }


    public static function afficherErreur(string $messageErreur = ""): void
    {
        if ($messageErreur == "") {
            $messageErreur = "Problème avec l'utilisateur.";
        } else {
            $messageErreur = "Problème avec l'utilisateur : " . $messageErreur;
        }

        // Appel de la vue d'erreur
        ControleurTrajet::afficherVue('vueGenerale.php', [
            'messageErreur' => $messageErreur,
            'titre' => 'Erreur utilisateur',
            'cheminCorpsVue' => 'trajet/erreur.php'
        ]);
    }

    public static function afficherDetail(): void
    {
        // Récupérer l'ID du trajet à partir de l'URL
        $trajetId = $_GET['id'];

        // Récupérer le trajet via le repository
        $trajet = (new TrajetRepository())->recupererTrajetParId($trajetId);

        ControleurTrajet::afficherVue('vueGenerale.php', [
            'trajet' => $trajet,
            'titre' => 'Détail du trajet',
            'cheminCorpsVue' => 'trajet/detail.php'
        ]);
    }

    public static function supprimerTrajet(): void
    {
        // Récupérer le login de l'utilisateur à supprimer
        $trajetId= $_GET['id'];

        // Suppression de l'utilisateur via le repository
        (new TrajetRepository())->supprimer($trajetId);

        // Récupérer la liste mise à jour des utilisateurs après suppression
        $trajets = (new TrajetRepository())->recuperer();

        // Afficher la vue utilisateurSupprime avec le login et la listeafficherFormulaireMiseAJour des utilisateurs
        ControleurTrajet::afficherVue('vueGenerale.php', [
            'trajets' => $trajets,
            'titre' => 'liste des trajets',
            'cheminCorpsVue' => 'trajet/trajetSupprime.php'
        ]);
    }
    public static function afficherFormulaireCreation(): void
    {
        ControleurTrajet::afficherVue('vueGenerale.php', [
            'titre' => 'Création d\'un trajet',
            'cheminCorpsVue' => 'trajet/formulaireCreation.php'
        ]);
    }

    public static function creerDepuisFormulaire(){

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['conducteurLogin']);

        $depart = $_GET['depart'];
        $arrivee = $_GET['arrivee'];
        $date = new DateTime($_GET['date']);
        $prix = $_GET['prix'];
        $conducteurLogin = $utilisateur;
        $nonFumeur = isset($_GET['nonFumeur']);

        $trajet = new Trajet(null, $depart, $arrivee, $date, $prix, $conducteurLogin, $nonFumeur, []);

        (new TrajetRepository())->ajouter($trajet);

        $trajets = (new TrajetRepository())->recuperer();

        ControleurTrajet::afficherVue('vueGenerale.php', [
            'trajets' => $trajets,
            'titre' => 'Liste des trajets',
            'cheminCorpsVue' => 'trajet/liste.php'
        ]);


    }
}

?>
