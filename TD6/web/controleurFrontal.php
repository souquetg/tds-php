<?php
use App\Covoiturage\Lib\Psr4AutoloaderClass;
use App\Covoiturage\Controleur\ControleurUtilisateur as ControleurUtilisateur;

require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';
// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');


if (isset($_GET['controleur'])) {
    $controleur = $_GET['controleur'];
} else {
    $controleur = 'utilisateur';
}

// On récupère l'action passée dans l'URL
if (isset($_GET['action'])) {
    $action = $_GET['action'];
} else {
    // Si aucune action n'est passée, on initialise avec "afficherListe"
    $action = 'afficherListe';
}

// Vérification si l'action existe dans ControleurUtilisateur
$nomDeClasseControleur = 'App\Covoiturage\Controleur\Controleur' . ucfirst($controleur);

// Vérifier si la classe contrôleur existe
if (class_exists($nomDeClasseControleur)) {
    // Vérifier si la méthode existe dans la classe contrôleur
    if (in_array($action, get_class_methods($nomDeClasseControleur))) {
        // Appel de la méthode action du contrôleur
        $nomDeClasseControleur::$action();
    } else {
        // Si l'action n'existe pas, appeler la méthode afficherErreur
        ControleurUtilisateur::afficherErreur("Action inconnue : $action");
    }
} else {
    // Si le contrôleur n'existe pas, appeler la méthode afficherErreur
    ControleurUtilisateur::afficherErreur("Contrôleur inconnu : $controleur");
}
?>
