<?php

use Modele\ConnexionBaseDeDonnees;

require_once 'Modele/ConnexionBaseDeDonnees.php';
require_once 'Modele/ModeleUtilisateur.php';

class Trajet {

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private Utilisateur $conducteur;
    private bool $nonFumeur;
    private array $passagers;
    public function __construct(
        ?int $id,
        string $depart,
        string $arrivee,
        DateTime $date,
        int $prix,
        Utilisateur $conducteur,
        bool $nonFumeur,
        array $passagers
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = $passagers;
    }

    public static function construireDepuisTableauSQL(array $trajetTableau) : Trajet {

        $trajettab = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]), // À changer
            $trajetTableau["prix"],
            Utilisateur::getUtilisateurParLogin($trajetTableau['conducteurLogin']), // À changer
            $trajetTableau["nonFumeur"],// À changer ?
            []
        );

        $trajettab->setPassagers($trajettab->recupererPassagers());
        return $trajettab;
    }

    public function supprimerPassager(string $passagerLogin): bool {
        $requete = "DELETE FROM passager WHERE passagerLogin = :passagerLogin AND trajetId = :trajetId";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($requete);

        $pdoStatement->execute([
            'passagerLogin' => $passagerLogin,
            'trajetId' => $this->id
        ]);

        return $pdoStatement->rowCount() > 0;
    }
    public static function recupererTrajetParId(int $trajetId): ?Trajet {
        $requete = "SELECT * FROM trajet WHERE id = :trajetId";
        $pdo = ConnexionBaseDeDonnees::getPDO()->prepare($requete);
        $pdo->execute(['trajetId' => $trajetId]);

        $trajetTableau = $pdo->fetch(PDO::FETCH_ASSOC);

        if ($trajetTableau) {
            // Retourne un trajet avec des passagers vides pour le moment
            return Trajet::construireDepuisTableauSQL($trajetTableau);
        } else {
            return null;
        }
    }

    public function ajouter() : bool
    {
        $requete = "INSERT INTO trajet (depart, arrivee, date, prix, conducteurLogin, nonFumeur) VALUES (:depart, :arrivee, :date, :prix, :conducteurLogin, :nonFumeur)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($requete);

        $pdoStatement->execute([
            'depart' => $this->depart,
            'arrivee' => $this->arrivee,
            'date' => $this->date->format("Y/m/d"),
            'prix' => $this->prix,
            'conducteurLogin' => $this->conducteur->getLogin(),
            'nonFumeur' => $this->nonFumeur,
        ]);

        try {
            $this->id = ConnexionBaseDeDonnees::getPDO()->lastInsertId();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @return Utilisateur[] qui retournera un tableau d’Utilisateur correspondant aux passagers du trajet courant en faisant la requête adéquate
     */
    private function recupererPassagers() : array {
        $tab = array();
        $requete = "
        SELECT u.* 
        FROM utilisateur u 
        INNER JOIN passager p ON u.loginBaseDeDonnees = p.passagerLogin 
        WHERE p.trajetId = :trajetId";

        $pdo = ConnexionBaseDeDonnees::getPDO()->prepare($requete);
        $values = array(
            'trajetId' => $this->id
        );

        $pdo->execute($values);
        foreach ($pdo as $passager) {
            $tab[] = Utilisateur::construireDepuisTableauSQL($passager);
        }

        return $tab;
    }

    public function getPassagers(): array
    {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): Utilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(Utilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : "";

        return "<p>
        Le trajet{$nonFumeur} du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).<br/>
    </p>";
    }

    /**
     * @return Trajet[]
     */
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }
}
