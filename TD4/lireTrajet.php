<?php
$trajets = Trajet::recupererTrajets();
foreach ($trajets as $trajet) {
    echo $trajet;
    $passagers = $trajet->getPassagers();
    foreach ($passagers as $passager) {
        echo "<p>Passager : {$passager->getPrenom()} {$passager->getNom()}";
        echo " <a href='supprimerPassager.php?login={$passager->getLogin()}&trajet_id={$trajet->getId()}'>Désinscrire</a></p>";
    }
}
?>