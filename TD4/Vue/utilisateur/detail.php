<?php
//affiche tout les détails de l'utilisateur stocké dans $utilisateur

/** @var ModeleUtilisateur $utilisateur */

use Modele\ModeleUtilisateur;

echo '<p> Utilisateur de login ' . $utilisateur->getLogin() . '.</p>';
echo '<p> Utilisateur de nom ' . $utilisateur->getNom() . '.</p>';
echo '<p> Utilisateur de prenom ' . $utilisateur->getPrenom() . '.</p>';

