<?php

use Modele\ModeleUtilisateur;

require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('../Vue/utilisateur/liste.php', ['utilisateurs' => $utilisateurs]);
    }

    /**
     * @throws Exception
     */
    public static function afficherDetail() : void {
        $login = $_GET['login'];
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login);
        if($utilisateur == NULL) {
            ControleurUtilisateur::afficherVue('../Vue/utilisateur/erreur.php');
        }else {
            ControleurUtilisateur::afficherVue('../Vue/utilisateur/detail.php',  ['utilisateur' => $utilisateur]);
        }
    }
    public static function afficherFormulaireCreation() : void {
        ControleurUtilisateur::afficherVue('../Vue/utilisateur/formulaireCreation.php');
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }

    public static function creerDepuisFormulaire() : void {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();
        ControleurUtilisateur::afficherListe();
    }


}
?>
