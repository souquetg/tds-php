<?php
require_once "ConnexionBaseDeDonnees.php";
require_once "ModeleUtilisateur.php";

$utilisateurs = Utilisateur::recupererUtilisateur();

// Affiche chaque utilisateur avec les trajet en tant que passager

foreach ($utilisateurs as $utilisateur) {
    echo $utilisateur . "<br>";
    foreach ($utilisateur->getTrajetsCommePassager() as $trajet) {
        echo "<p>Trajet de {$utilisateur->getLogin()} : {$trajet->getDepart()} {$trajet->getArrivee()}</p>";
    }
}

?>

